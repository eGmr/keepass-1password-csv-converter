# keepass-1password-csv-converter

A simple Kotlin application to convert KeePass CSV export to a format which 1Password accepts.

Quick creation, with basic unit tests, which help to verify the expected behaviors.
Getters and setters generally are not tested if they do not have any logic in it.
CSV Writer was not tested as basically it is simply using FileWriter, and it did not add much value to test this class.

Feel free to fork and extend as your own mini project, just to learn and play with Kotlin, or extend and create your own converter from this.

## Assumptions

Contents of Header in KeePass CSV file should always be the same.

Only basic login data are migrated over from one CSV format to another.

Example Input -> Output (1Password)
- Account -> title
- Login Name -> username
- Password -> password
- Web Site -> website
- Comments -> notes

## Usage

Update `App.kt` with the necessary file for reading and writing, then run main.
File paths specified should be based off project root. 

### Supported Input Files

- KeePass CSV (1.x)

### Output Files

- CSV for importing login data in 1Password

### Improvements to be made

- Validate header of supported KeePass CSV Input
- Refactor processing of multi-line data to CSV Writer, instead of having it only for Notes
