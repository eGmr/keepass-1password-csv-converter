import csvReaders.KeePassCsvReader
import csvWriters.OnePasswordCsvWriter

fun main() {
    val csvReader = KeePassCsvReader()
    val csvWriter = OnePasswordCsvWriter()
    println("Welcome to new CSV converter")
    val keePassData = csvReader.readDataFromCsv("src/main/resources/Sample.csv")
    csvWriter.writeToCSV(keePassData, "out/OnePassword.csv")
}