package model.interfaces

interface OnePasswordCSV {
    fun getTitle(): String
    fun getUsername(): String
    fun getPassword(): String
    fun getWebsite(): String?
    fun getNotes(): String?
}