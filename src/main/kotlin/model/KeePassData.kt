package model

import model.interfaces.Data

data class KeePassData(
    val account: String,
    val login: String,
    val passWord: String,
    val webSite: String? = null,
    val comment: String? = null
) : Data {
    override fun getTitle(): String {
        return account
    }

    override fun getUsername(): String {
        return login
    }

    override fun getPassword(): String {
        return passWord
    }

    override fun getWebsite(): String? {
        return webSite
    }

    override fun getNotes(): String? {
        return comment?.let {
            when {
                comment.contains('\n') -> {
                    "\"$comment\""
                }
                else -> {
                    comment
                }
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as KeePassData

        if (account != other.account) return false
        if (login != other.login) return false
        if (passWord != other.passWord) return false
        if (webSite != other.webSite) return false
        if (comment != other.comment) return false

        return true
    }
}