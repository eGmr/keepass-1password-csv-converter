package csvWriters.interfaces

import model.interfaces.Data

interface CSVWriter {
    fun writeToCSV(dataList: List<Data>, outputFile: String)
}