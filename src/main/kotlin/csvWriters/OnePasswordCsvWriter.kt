package csvWriters

import csvWriters.interfaces.CSVWriter
import model.interfaces.Data
import java.io.FileWriter

class OnePasswordCsvWriter(private val delimiter: Char = ',') : CSVWriter {

    override fun writeToCSV(dataList: List<Data>, outputFile: String) {
        val fileWriter = FileWriter(outputFile, Charsets.UTF_8)
        fileWriter.append(HEADER)
        fileWriter.append(NEW_LINE)
        for (data in dataList) {
            appendDataToFileWriter(data, fileWriter)
            fileWriter.append(NEW_LINE)
        }
        fileWriter.flush()
        fileWriter.close()
    }

    private fun appendDataToFileWriter(data: Data, fileWriter: FileWriter) {
        fileWriter.append(data.getTitle())
        fileWriter.append(delimiter)
        fileWriter.append(data.getWebsite())
        fileWriter.append(delimiter)
        fileWriter.append(data.getUsername())
        fileWriter.append(delimiter)
        fileWriter.append(data.getPassword())
        fileWriter.append(delimiter)
        fileWriter.append(data.getNotes())
    }

    companion object {
        private const val HEADER = "title,website,username,password,notes"
        private const val NEW_LINE = "\n"
    }
}