package csvReaders.interfaces

import model.interfaces.Data

interface CSVReader {
    fun readDataFromCsv(filename: String): List<Data>
}