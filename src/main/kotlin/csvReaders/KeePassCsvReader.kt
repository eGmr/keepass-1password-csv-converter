package csvReaders

import csvReaders.interfaces.CSVReader
import model.KeePassData
import java.io.BufferedReader
import java.io.FileReader
import java.lang.Exception

class KeePassCsvReader : CSVReader {
    private lateinit var fileReader: BufferedReader

    override fun readDataFromCsv(filename: String): List<KeePassData> {
        try {
            fileReader = BufferedReader(FileReader(filename))
            val header = fileReader.readLine()
            var dataRow = readLineThatEndsWithQuote()
            val output = arrayListOf<KeePassData>()
            if (header.isNullOrEmpty() || dataRow.isNullOrEmpty()) {
                return output
            }
            do {
                val tokens = dataRow!!.split(',')
                output.add(
                    KeePassData(
                        tokens[ACCOUNT_INDEX].trim().removeSurroundingQuotes(),
                        tokens[LOGIN_INDEX].trim().removeSurroundingQuotes(),
                        tokens[PASSWORD_INDEX].trim().removeSurroundingQuotes(),
                        tokens[WEBSITE_INDEX].trim().removeSurroundingQuotes(),
                        tokens[COMMENTS_INDEX].trim().removeSurroundingQuotes()
                    )
                )
                dataRow = readLineThatEndsWithQuote()
            } while (dataRow != null)
            return output
        } catch (e: Exception) {
            throw e
        }
    }

    private fun String.removeSurroundingQuotes(): String {
        return this.replace(Regex("^\""), "").replace(Regex("\"$"), "")
    }

    private fun readLineThatEndsWithQuote(): String? {
        var line = fileReader.readLine()
        while (line != null && line[line.lastIndex] != '"') {
            line += "\n"
            val nextLine = fileReader.readLine()
            nextLine.let { line += it }
        }
        return line
    }

    companion object {
        //TODO: Add Constant to Validate Header Line
        private const val ACCOUNT_INDEX = 0
        private const val LOGIN_INDEX = 1
        private const val PASSWORD_INDEX = 2
        private const val WEBSITE_INDEX = 3
        private const val COMMENTS_INDEX = 4
    }
}