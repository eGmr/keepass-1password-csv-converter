package model

import org.junit.Assert.*
import org.junit.Test

class KeePassDataTest {
    @Test
    fun `getNotes should return only content`() {
        val testData = KeePassData("Test", "test", "pass", null, "a Comment")
        assertEquals("a Comment", testData.getNotes())
    }

    @Test
    fun `getNotes should return null when content is null`() {
        val testData = KeePassData("Test", "test", "pass", null, null)
        assertEquals(null, testData.getNotes())
    }

    @Test
    fun `getNotes should return add quotes when content is multi-lined`() {
        val testData = KeePassData("Test", "test", "pass", null, "Hello\n\nWorld")
        assertEquals("\"Hello\n\nWorld\"", testData.getNotes())
    }
}