package csvReaders

import model.KeePassData
import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import java.io.FileNotFoundException
import java.lang.Exception

class KeePassCsvReaderTest {
    private val kpCsvReader = KeePassCsvReader()

    @Test
    fun `should throw exception when file not found`() {
        try {
            kpCsvReader.readDataFromCsv("src/test/resources/missing.csv")
            fail()
        } catch (e: Exception) {
            assertTrue(e is FileNotFoundException)
        }
    }

    @Test
    fun `should return emptyList when csv is empty`() {
        assertTrue(kpCsvReader.readDataFromCsv("src/test/resources/empty.csv").isEmpty())
    }

    @Test
    fun `should return emptyList when csv only contains header row`() {
        assertTrue(kpCsvReader.readDataFromCsv("src/test/resources/headerOnly.csv").isEmpty())
    }

    @Test
    fun `should process a row of data correctly`() {
        assertEquals(
            listOf(
                KeePassData("Test Acc", "test", "test_pass", "test.com", "testing 1 2 3")
            ),
            kpCsvReader.readDataFromCsv("src/test/resources/singleData.csv")
        )
    }

    @Test
    fun `should process a row of multi-line data correctly`() {
        assertEquals(
            listOf(
                KeePassData(
                    account = "Multi-Line Test", login = "test", passWord = "test_pass", webSite = "test.com", comment = """This is a multi-line comment.
This is a multi-line comment.
This is a multi-line comment.
This is a multi-line comment.
This is a multi-line comment.
This is a multi-line comment.
This is a multi-line comment."""
                )
            ),
            kpCsvReader.readDataFromCsv("src/test/resources/singleMultiLineData.csv")
        )
    }

    @Test
    fun `should process a row of data with special characters correctly`() {
        assertEquals(
            listOf(
                KeePassData(
                    account = "Entry To Test Special Characters",
                    login = "!\\\"§$%&/()=?´`_#²³{[]}\\\\",
                    passWord = "öäüÖÄÜß€@<>µ©®",
                    webSite = "http://www.website.com",
                    comment = "The user name and password fields contain special characters."
                )
            ),
            kpCsvReader.readDataFromCsv("src/test/resources/specialCharTest.csv")
        )
    }
}